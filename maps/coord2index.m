function [ID] = coord2index(x,y,resolution,minx,miny)
%COORD2INDEX Summary of this function goes here
%   Detailed explanation goes here
ID = [round(((x-minx)/resolution)+1),round(((y-miny)/resolution)+1)];
end
function [ p ] = GetUnInterestinGivenUninterestingProbability( range )

y = [0.5,0.5,0.99,0.95,0.7,0.6,0.5,0.5,0.5];
x = [0,10,10.1,20,40,70,150,180,350];
p = interp1(x,y,range,'linear','extrap');

end


function [ meas ] = GetBinaryStochasticMeasurements( ground_truth, p_ground_truth)
%STOCHASTICMEASUREMENTS Summary of this function goes here
%   Detailed explanation goes here
s = rand(size(ground_truth));
meas = ground_truth;
meas(s>p_ground_truth) = 1-meas(s>p_ground_truth);
end


function [ info_gain , last_evaluation ] = GetInformationGainIncremental( node_indices,pose, world_map, belief_map, map_struct, sensor_params )
%GETINFORMATIONGAININCREMENTAL Summary of this function goes here
%
% PLEASE READ THIS
% The things that are checked for changes are
%   pose
%   node indices
%   belief_map
if(size(node_indices,1)==1)
    node_indices = node_indices';
end

if isempty(node_indices)
    last_evaluation = [];
    info_gain = [];
    return;
end
persistent incremental_last_evaluation;

if isempty(incremental_last_evaluation)
    incremental_last_evaluation.last_set_evaluated = [];
    incremental_last_evaluation.initial_conditions = [];
    incremental_last_evaluation.initial_conditions.belief_map = belief_map;
    incremental_last_evaluation.pose = pose;
    incremental_last_evaluation.info_gain = [];
    incremental_last_evaluation.info_gain = 0;
    recomputation_index = 1;
end

same_initial_conditions = isequal(belief_map,incremental_last_evaluation.initial_conditions(1).belief_map);
same_initial_conditions = same_initial_conditions && isequal(pose,incremental_last_evaluation.pose);
% Are the initial conditions same?
if same_initial_conditions
   % find how much of the previous calculation can we use
    for i=1:size(incremental_last_evaluation.last_set_evaluated,1)
        if i>size(node_indices,1)
            recomputation_index = max(i,1);
            break;
        end
        if incremental_last_evaluation.last_set_evaluated(i) ~= node_indices(i)            
            recomputation_index = i;
            break;    
        else
            recomputation_index = i+1; % this saves us from an edge case
        end    
    end
else
    recomputation_index = 1;
    incremental_last_evaluation.info_gain = [];
    incremental_last_evaluation.info_gain = 0;
    incremental_last_evaluation.initial_conditions = [];
    incremental_last_evaluation.initial_conditions.belief_map = belief_map;
end
    

if recomputation_index > size(node_indices,1)
    info_gain = incremental_last_evaluation.info_gain(size(node_indices,1));
    last_evaluation = incremental_last_evaluation;    
    return;
end

incremental_last_evaluation.last_set_evaluated = node_indices;
incremental_last_evaluation.pose = pose;
incremental_last_evaluation.initial_conditions(recomputation_index+1:end) = [];
incremental_last_evaluation.info_gain(recomputation_index:end,:) = [];    
%fprintf('Query Set Size:%i ', size(node_indices,1))
node_indices = node_indices(recomputation_index:end,:);

%fprintf('Incremental Change Size:%i \n', size(node_indices,1))
pose = pose(node_indices(:),:);
b_map = incremental_last_evaluation.initial_conditions(end).belief_map;


[ ~, incremental_conditions ] = GetInformationGain( pose, world_map, b_map, map_struct, sensor_params, belief_map);

incremental_last_evaluation.initial_conditions(recomputation_index:end,:) = [];
if size(incremental_last_evaluation.info_gain,1)==0
    incremental_last_evaluation.info_gain = incremental_conditions.info_gain;
else
    incremental_last_evaluation.info_gain = [incremental_last_evaluation.info_gain;
                                        (incremental_conditions.info_gain+incremental_last_evaluation.info_gain(end))];
end

info_gain = incremental_last_evaluation.info_gain(end);

incremental_last_evaluation.initial_conditions = [incremental_last_evaluation.initial_conditions;
                                                   incremental_conditions.initial_conditions];                                                
last_evaluation = incremental_last_evaluation;
end
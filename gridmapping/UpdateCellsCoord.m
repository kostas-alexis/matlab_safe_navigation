function [ map ] = UpdateCellsCoord( values, map, coords, map_struct)
%   Detailed explanation goes here
[ID, valid] = getValidIds(coords, map, map_struct);

ID = ID(valid>0,:);
values = values(valid>0);
X = ID(:,1); Y = ID(:,2);
IND = sub2ind(size(map),X,Y);
map(IND(:)) = values;
end
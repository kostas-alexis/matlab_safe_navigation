function [ probability ] = probabilitySafeTrajectorySet( setIds,pathList,pathIds,nonAliasedIndices,pathMap,occMap )
%PROBABILITYSAFE Summary of this function goes here
%   Detailed explanation goes here
probability = 0;

for i=1:size(setIds,1)
    probabilityPathIntersection = 0;
    probabilityPathIntersection3 = 0;
    pathInds = nonAliasedIndices(setIds(i)).indices;
    probabilityPathAlone = getSafeProbabilityIndices(pathInds,occMap);
    for j=i+1:size(setIds,1)
        pathInds2 = unique([nonAliasedIndices(setIds(j)).indices;pathInds]);
        probabilityPathIntersection = probabilityPathIntersection + getSafeProbabilityIndices(pathInds2,occMap);
    end
    
%     for j=i+1:size(setIds,1)
%         pathInds2 = unique([nonAliasedIndices(setIds(j)).indices;pathInds]);
%         for k=j+1:size(setIds,1)
%             pathInds2 = unique([nonAliasedIndices(setIds(k)).indices;pathInds2]);
%             probabilityPathIntersection3 = probabilityPathIntersection3 + getSafeProbabilityIndices(pathInds2,occMap);
%         end
%     end
    
    probability = probability + probabilityPathAlone - probabilityPathIntersection;
end


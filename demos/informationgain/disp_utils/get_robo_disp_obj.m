function display_obj = get_robo_disp_obj(state, map, display_obj)
%GET_INFORMATION_DISP_OBJ Summary of this function goes here
%   Detailed explanation goes here
if (nargin<3)
    display_obj = struct();
    display('Error get_robo_disp_obj needs a display_obj with maps.')
    return;
end
% Figure 1: Robo display
if(~isempty(display_obj.handle_set.robo_state))
    delete(display_obj.handle_set.robo_state);
end
set(0,'CurrentFigure',display_obj.handle_set.fig_robo_view);
hold on;
triangle_params.base = max(size(map.data))*0.05;
triangle_params.height = triangle_params.base*1.2;
display_obj.handle_set.robo_state = plot_robot( state, triangle_params, map);
axis xy;
axis equal;

% Figure 2: WorldMap display
if(~isempty(display_obj.handle_set.world_robo_state))
    delete(display_obj.handle_set.world_robo_state);
end
set(0,'CurrentFigure',display_obj.handle_set.fig_world);
hold on;
display_obj.handle_set.world_robo_state = plot_robot( state, triangle_params, map);
axis xy;
axis equal;

end


clear all
disp(' ')
disp('---------------------------------------------------------');
disp('Now let''s use the pSPIEL algorithm to trade off informativeness and travelling cost')
disp(' ');
do_plot = 1;
addpath ../../SFO/sfo
addpath ../../maps
addpath ../../gridmapping
addpath ../../infoGain
addpath ../../semanticClassificationModel
addpath ../../explorationGridRepresentation
addpath ../../cameraModels
addpath ../cameraBasedSemanticGridRepresentation/utils
addpath ./maps
addpath ./utils
addpath ./incremental_set_evaluation
addpath ./utils/temp
addpath ../rhcp/utils
addpath ../informationgain/disp_utils
addpath ../../normalPathLibrary
%% getting map
grid_params = get_grid_params('semantic_camera_exploration_grid');
[ world_map, belief_map ] = get_map_obj( 'buildings.png', grid_params );
world_map.data = 1-world_map.data;

%% getting sensor params
camera_params = GetCameraParams('circular_120');

%% setting up the map
ground_truth_log_odds = prob2logodds(world_map.data);
bounding_box.min_x = 1;
bounding_box.min_y = 1;
bounding_box.max_x = size(world_map.data,1);
bounding_box.max_y = size(world_map.data,2);
bounding_box.mode = 'gt';
bounding_box.value = 0;
% building 1
bounding_box(2).min_y = 289;
bounding_box(2).min_x = 268;
bounding_box(2).max_y = 375;
bounding_box(2).max_x = 389;
bounding_box(2).mode = '';
bounding_box(2).value = prob2logodds(0.7);
% building 2
bounding_box(3).min_y = 477;
bounding_box(3).min_x = 451;
bounding_box(3).max_y = 556;
bounding_box(3).max_x = 486;
bounding_box(3).mode = '';
bounding_box(3).value = prob2logodds(0.7);
% building 3
bounding_box(4).min_y = 451;
bounding_box(4).min_x = 501;
bounding_box(4).max_y = 496;
bounding_box(4).max_x = 554;
bounding_box(4).mode = '';
bounding_box(4).value = prob2logodds(0.7);
% building 4
bounding_box(5).min_y = 551;
bounding_box(5).min_x = 542;
bounding_box(5).max_y = 722;
bounding_box(5).max_x = 695;
bounding_box(5).mode = '';
bounding_box(5).value = prob2logodds(0.7);

[ belief_map.data ] = setup_map( bounding_box, ground_truth_log_odds, belief_map.data, world_map);

%% making nodes
bin_size = 200;
max_cell = 1000;
[X,Y] = meshgrid(bin_size/2:bin_size:max_cell,bin_size/2:bin_size:max_cell);
pose = [X(:),Y(:),60*ones(size(X(:)))];

pose1 = pose+(bin_size/4-1)*rand(size(pose));
pose2 = pose+(bin_size/4-1)*rand(size(pose));
pose1(:,3) = 40;
pose2(:,3) = 20;
pose = [pose;pose1;pose2];

V = [1:size(pose,1)]';
D = zeros(size(V,1));
B = 1500;
R = bin_size;
Vroot = 1;
for i=1:size(pose,1)
    D(i,i) = 0;
 for j=i+1:size(pose,1)
    distance =  pose(i,:) - pose(j,:);
    distance = sqrt(sum(distance.*distance));
    D(i,j) = distance;
    D(j,i) = distance;
 end
end

F = @(x) GetInformationGainIncremental(x, pose, belief_map.data, belief_map.data, world_map, camera_params );


disp('[A E result] = sfo_pspiel_orienteering(F,V,B,D,maxIter,R)');
maxIter = 10;
params.num_nodes_per_bin = 3;
bin_size = 200;
pd = get_padded_decomposition( pose,bin_size,'binning',params);
tic
[A E result] = sfo_pspiel_orienteering_fixed_pd(F,V,B,D,maxIter,Vroot,pd); 
toc

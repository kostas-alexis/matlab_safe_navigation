function robo_map = update_belief( robo_map, state, world_map, laser_params, grid_params, display_obj)
%UPDATE_BELIEF Summary of this function goes here
%   Detailed explanation goes here

[robo_map.trace_x, robo_map.trace_y, robo_map.trace_r, robo_map.trace_collision] = ...
    fast_trace(world_map, laser_params.laser_fov, laser_params.laser_resolution, laser_params.max_range, laser_params.range_resolution, ...
    [state.x state.y], state.psi);

robo_map.data = update_robo_world(laser_params.laser_fov, laser_params.laser_resolution, laser_params.range_resolution, ...
                             robo_map.trace_x, robo_map.trace_y, robo_map.trace_r, robo_map.trace_collision, ...
                             [state.x state.y], state.psi, robo_map.data, world_map, grid_params);
                         
[ robo_map.p, robo_map.p_ ] = logodds2prob( robo_map.data );
set(display_obj.handle_set.occ_map,'CData', robo_map.p_');

end


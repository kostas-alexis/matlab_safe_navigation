function [chosen_path, valid] = select_plan(  state, problem, robo_map, planner_params, display_obj, learner_params )
%SELECT_PLAN Summary of this function goes here
%   Detailed explanation goes here

% Update heuristic
threshold_map = robo_map.p_;
threshold_map(threshold_map < 0.1) = 0;
threshold_map(threshold_map > 0.1) = 1;
heur = get_value_function( threshold_map, problem.goal.x, problem.goal.y );
set(display_obj.handle_set.heur_map,'CData', heur');

% Tranform path to robot state
tf_path_set = transform_path_set(  state, planner_params.path_set );
display_obj.handle_set.path_set = plot_path_set( tf_path_set, display_obj.handle_set.path_set );
display_obj.handle_set.world_path_set = plot_path_set( tf_path_set, display_obj.handle_set.world_path_set );    

% Sort path by: f_cost + f_heuristic
criteria = [];
for i=1:length(tf_path_set)
    if (nargin > 5)
        p_total = learner_params.get_collision_prob( learner_params.weight, learner_params.feature(tf_path_set(i), robo_map));
        criteria = [criteria; i p_total*planner_params.j_coll + (1-p_total)*planner_params.f_cost(tf_path_set(i)) + planner_params.f_heur( heur, tf_path_set(i).x(end), tf_path_set(i).y(end))];
    else
        criteria = [criteria; i planner_params.f_cost(tf_path_set(i)) + planner_params.f_heur( heur, tf_path_set(i).x(end), tf_path_set(i).y(end))];
    end
end
criteria = sortrows(criteria,2);
tf_path_set = tf_path_set(round(criteria(:,1)));

% Select best collision free trajectory
chosen_path = [];
for i = 1:length(tf_path_set)
    collision = false;
    for j = 1:length(tf_path_set(i).x)
        if (planner_params.f_coll(threshold_map, tf_path_set(i).x(j), tf_path_set(i).y(j)))
            collision = true;
            break;
        end
    end
    if (~collision)
        chosen_path = tf_path_set(i);
        break;
    end
end

% If no path then failed
if(isempty(chosen_path))
    valid = false;
else
    valid = true;
    display_obj.handle_set.chosen = plot_path( chosen_path, display_obj.handle_set.chosen);
    display_obj.handle_set.world_chosen = plot_path( chosen_path, display_obj.handle_set.world_chosen);
end


    
end

